(() => {
	let a = document.createElement("a");
	let listenerName = 
		document.querySelector("#conversationHead .name").innerHTML;
	a.href = 
		"data:text/html," +
		Array.from(document.querySelectorAll(".convRow"))
		.map(x => (
				(e = x.querySelector(".clearfix").children[1])
				.outerHTML.replace("</div>", " <em class='time'>") + (
					(me = e.classList.contains("meWrap")) 
						? e.parentNode.querySelector(".msgTime").innerHTML 
						: e.parentNode.querySelector(".details").innerHTML
							.trim().replace(/<span.*/, "")
				) + "</em></div>"
			).replace(">", 
				"><strong class='name'>" 
				+ (me ? "Me" : listenerName) + " </strong>"
			)
		).join("\n");
	let d = new Date();
	a.download = 
		d.getUTCFullYear() + '-' + (String(d.getUTCMonth() + 1).padStart(2, "0")) 
		+ '-' + d.getUTCDate() + "_" + listenerName + ".html";
	a.click();
})();
